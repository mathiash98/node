﻿var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

var connected = 0;

app.use(express.static(__dirname + '/bower_components'));
app.get('/', function (req, res, next) {
    res.sendFile(__dirname + '/index.html');
});
app.get('/box', function (req, res, next) {
    res.sendFile(__dirname + '/box.html');
});

app.get('/cat', function (req, res, next) {
    res.sendFile(__dirname + '/cat.html');
});

//Box
var box_pos;

//Socket.io stuff
io.on('connection', function (client) {
    connected++;
    console.log('Client connected... There are ' + connected + ' user(s) online!');

    client.on('chat message', function (msg){
        console.log('Message: ' + msg);
        io.emit('chat message', msg);
    });
   
    //Box
    client.on('box_pos', function (pos) {
        /*
        box_pos = pos;
        console.log(box_pos);
         */
        client.broadcast.emit('box_pos', pos);
    });
    client.on('box2_pos', function (pos) {
        client.broadcast.emit('box2_pos', pos);
    });


    client.on('disconnect', function () {
        connected--;
        console.log('User disconnected there are ' + connected + ' user(s) online!');
    });
});

server.listen(80);